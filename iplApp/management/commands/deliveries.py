from django.core.management.base import BaseCommand, CommandError
from iplApp.models import  Deliveries


class Command(BaseCommand):

    def handle(self, *args, **options):
        Deliveries.objects.from_csv('deliveries.csv')
        self.stdout.write(self.style.SUCCESS('Successfully copied from csv'))