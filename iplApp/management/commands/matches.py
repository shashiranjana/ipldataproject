from django.core.management.base import BaseCommand, CommandError
from iplApp.models import Matches

class Command(BaseCommand):

    def handle(self, *args, **options):
        Matches.objects.from_csv('matches.csv')
        self.stdout.write(self.style.SUCCESS('Successfully copied from csv'))