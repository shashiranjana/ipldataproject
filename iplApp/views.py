from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from iplApp.models import Matches, Deliveries
from django.db.models import *
from django.db.models import Sum
from django.db.models.functions import Cast
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.views.decorators.cache import cache_page
import json
import urllib.parse
import urllib.request
from rest_framework.views import APIView
from . serializer import matches_serializers, deliveries_serailizers
from django.core.paginator import Paginator

# Create your views here.

CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)

# @cache_page(CACHE_TTL)
def index(request):
    return render(request, 'iplApp/index.html')

# api for matche per year
@cache_page(CACHE_TTL)
def api_matches_per_year(request):
    matches_per_year = {}
    matches_per_year_obj = Matches.objects.values('season').annotate(Count('season'))
    matches_per_year['data'] = list(matches_per_year_obj)
    return JsonResponse(matches_per_year)

# template for matches per year
def matches_per_year(request):
    return render(request, 'iplApp/matches_per_year.html')

#api for matches won by team
# @cache_page(CACHE_TTL)
def api_matches_won_by_team(request):
    matches_won_by_team = {}
    matches_won_by_team_obj = Matches.objects.values('winner','season').annotate(win=Count('winner')).order_by('season','winner')

    matches_won_by_team['data'] = list(matches_won_by_team_obj)

    return JsonResponse(matches_won_by_team)

#template for matches won by tema
def matches_won_by_team(request):
    return render(request, 'iplApp/matches_won_by_team.html')

# api for extra runs conceded by team in 2016
@cache_page(CACHE_TTL)
def api_extra_runs_conceded_by_team_in_2016(request):
    extra_runs_conceded_by_team_in_2016= {}
    extra_runs_conceded_by_team_in_2016_obj =Deliveries.objects.values('bowling_team').filter(match_id__season=2016).annotate(Sum('extra_runs'))
    extra_runs_conceded_by_team_in_2016['data'] = list(extra_runs_conceded_by_team_in_2016_obj)
    return JsonResponse(extra_runs_conceded_by_team_in_2016)

#template for extra runs conceded by team in 2016
@cache_page(CACHE_TTL)
def extra_runs_conceded_by_team_in_2016(request):
    return render(request, 'iplApp/extra_runs_conceded_by_team_in_2016.html')

#api for top economical bowlers in 2015
@cache_page(CACHE_TTL)
def api_top_economical_bowlers_2015(request):
    top_economical_bowlers_2015 = {}
    top_economical_bowlers_2015_obj = Deliveries.objects.filter(match_id__season = 2015, is_super_over=False).values('bowler').annotate(runs=Sum('batsman_runs')+Sum('wide_runs')+Sum('noball_runs')).annotate(balls=Count('ball')-Count(Case(When(noball_runs__gt=0, then=1)))-Count(Case(When(wide_runs__gt=0, then=1)))).annotate(economy=Cast((F('runs')/(F('balls')/6.0)), FloatField())).order_by('economy')[:10]

    top_economical_bowlers_2015['data'] = list(top_economical_bowlers_2015_obj)

    return JsonResponse(top_economical_bowlers_2015)

#template for top economical bowlers
@cache_page(CACHE_TTL)
def top_economical_bowlers_2015(request):
    return render(request,'iplApp/top_economical_bowlers_2015.html')

#api for batting average
# @cache_page(CACHE_TTL)
def api_batting_average(request):
    batting_avg = {}
    batting_avg_obj = Deliveries.objects.values('batsman').annotate(avg=Sum('batsman_runs')).order_by('-avg')

    batting_avg['data'] = list(batting_avg_obj)
    return JsonResponse(batting_avg)

#template for batting average
@cache_page(CACHE_TTL)
def batting_average(request):
    return render(request,'iplApp/batting_average.html')

#********** Api for Movies *****************
@csrf_exempt
def api_for_matches(request):
    if request.method == 'GET':
        matches_details= Matches.objects.all()
        matches_details_value = list(matches_details.values())

        return JsonResponse(matches_details_value, safe=False)

    elif request.method == 'POST':
        data = request.body.decode(encoding='utf-8')
        data = json.loads(data)
        match_id = len(Matches.objects.all())+1
        print(match_id)
        matches = Matches(id=match_id, season=data['season'],city=data['city'], date=data['date'], team1=data['team1'], team2=data['team2'], toss_winner=data['toss_winner'], toss_decision=data['toss_decision'], result=data['result'], dl_applied=data['dl_applied'], winner=data['winner'], win_by_runs=data['win_by_runs'], win_by_wickets=data['win_by_wickets'], player_of_match=data['player_of_match'], venue=data['venue'], umpire1=data['umpire1'], umpire2=data['umpire2'], umpire3=data['umpire3'])
        matches.save()
        return JsonResponse({"status":"match submitted"}, status=200)


@csrf_exempt
def match_details_using_id(request,match_id):
    if request.method == 'GET':
        match_details = Matches.objects.filter(id=match_id)
        matches_details_value = list(match_details.values())
        return JsonResponse(matches_details_value, safe=False)

    if request.method == 'PUT':
        matches_data = Matches.objects.get(id = match_id)
        request_body = request.body.decode(encoding = 'utf-8')
        request_data = json.loads(request_body)
        for key in request_data:
            if hasattr(matches_data, key):
                print(key)
                setattr(matches_data, key, request_data[key])
            else:
                raise Exception('invalid data')
        matches_data.save()
        return JsonResponse({'status' : 203})

    if request.method ==  'DELETE':
        Matches.objects.filter(id=match_id).delete()


#*****************API for Deliveries ********************
@csrf_exempt
def api_for_deliveries(request):
    if request.method == 'GET':
        deliveries_details= Deliveries.objects.all()
        deliveries_details_value = list(deliveries_details.values())

        return JsonResponse(deliveries_details_value, safe=False)

    elif request.method == 'POST':
        data = request.body.decode(encoding='utf-8')
        data = json.loads(data)
        deliveries = Deliveries(match_id=data['match_id'],inning=data['inning'],batting_team=data['batting_team'],bowling_team=data['bowling_team'],over=data['over'],ball=data['ball'],batsman=data['batsman'],non_striker=data['non_striker'],bowler=data['bowler'],is_super_over=data['is_super_over'],wide_runs=data['wide_runs'],bye_runs=data['bye_runs'],legbye_runs=data['legbye_runs'],noball_runs=data['noball_runs'],penalty_runs=data['penalty_runs'],batsman_runs=data['batsman_runs'],extra_runs=data['extra_runs'],total_runs=data['total_runs'],player_dismissed=data['player_dismissed'],dismissal_kind=data['dismissal_kind'],fielder=data['fielder'])
        deliveries.save()
        return JsonResponse({"status":"deliveries submitted"}, status=200)



@csrf_exempt
def deliveries_details_using_id(request,match_id):
    if request.method == 'GET':
        match_details = Deliveries.objects.filter(id=match_id)
        deliveries_details_value = list(match_details.values())
        return JsonResponse(deliveries_details_value, safe=False)

    if request.method == 'PUT':
        deliveries_data = Deliveries.objects.get(id = match_id)
        request_body = request.body.decode(encoding = 'utf-8')
        request_data = json.loads(request_body)
        for key in request_data:
            if hasattr(deliveries_data, key):
                print(key)
                setattr(deliveries_data, key, request_data[key])
            else:
                raise Exception('invalid data')
        deliveries_data.save()
        return JsonResponse({'status' : 203})

    if request.method ==  'DELETE':
        Deliveries.objects.filter(id=match_id).delete()
        return JsonResponse({"status":"deleted "}, status=200)


#*************** Matches API using DRF ******************************


class matchesList(APIView):

    def get(self,request):
        matches = Matches.objects.all()
        serializer = matches_serializers(matches, many=True)
        return JsonResponse(serializer.data, safe=False)

#*************** Deliveries API using DRF ******************************

class deliveriesList(APIView):

    def get(self,request):
        deliveries = Deliveries.objects.all()
        serializer = deliveries_serailizers(deliveries, many=True)
        return JsonResponse(serializer.data, safe=False)

