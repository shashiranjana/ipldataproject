from django.urls import path
from . import views

urlpatterns = [

    path('index',views.index,name='index'),
    path('api/v1/api_matches_per_year', views.api_matches_per_year, name='api_matches_per_year' ),
    path('matches_per_year', views.matches_per_year, name='matches_per_year' ),
    path('api/v1/api_matches_won_by_team', views.api_matches_won_by_team, name='api_matches_won_by_team' ),
    path('matches_won_by_team', views.matches_won_by_team, name='matches_won_by_team' ),
    path('api/v1/api_extra_runs_conceded_by_team_in_2016', views.api_extra_runs_conceded_by_team_in_2016, name='api_extra_runs_conceded_by_team_in_2016' ),
    path('extra_runs_conceded_by_team_in_2016', views.extra_runs_conceded_by_team_in_2016, name='extra_runs_conceded_by_team_in_2016' ),
    path('api/v1/api_top_economical_bowlers_2015', views.api_top_economical_bowlers_2015, name='api_top_economical_bowlers_2015' ),
    path('top_economical_bowlers_2015', views.top_economical_bowlers_2015, name='top_economical_bowlers_2015' ),
    path('api/v1/api_batting_average', views.api_batting_average, name='api_batting_average' ),
    path('batting_average', views.batting_average, name='batting_average' ),
    path('matches',views.api_for_matches,name='api_for_matches'),
    path('matches/<match_id>',views.match_details_using_id,name='match_details_using_id'),
    path('deliveries',views.api_for_deliveries,name='api_for_deliveries'),
    path('deliveries/<match_id>',views.deliveries_details_using_id,name='deliveries_details_using_id'),

    path('match', views.matchesList.as_view()),
    path('delivery', views.deliveriesList.as_view())

]