from rest_framework import serializers
from . models import Matches, Deliveries

class matches_serializers(serializers.ModelSerializer):

    class Meta:
        model = Matches
        fields = '__all__'

class deliveries_serailizers(serializers.ModelSerializer):

    class Meta:
        model = Deliveries
        fields = '__all__'

