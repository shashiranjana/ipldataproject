<h1>ipl data project using django</h1>
<h3>Explain:</h3>
This data assignment you will transform raw data from IPL into graphs that will convey some meaning / analysis. For each part of this assignment you will have 2 parts -
Dataset: Download both csv files from https://www.kaggle.com/manasgarg/ipl
<br>
<h3>a.Create API route for all below questions</h3>
<h3>b.Plot for all questions using Highchart</h3>

Generate the following plots ...
<br>
<h4>
Plot the number of matches played per year of all the years in IPL.</h4>
<h4>
Plot a stacked bar chart of matches won of all teams over all the years of IPL.</h4>
<h4>
For the year 2016 plot the extra runs conceded per team.</h4>
<h4>
For the year 2015 plot the top economical bowlers.</h4>
<h3>Cache Using Redis</h3>

<h2>API Route</h2>
<h4>Api route for all matches. GET, POST,PUT, DELETE</h4>
<h4>Api route for all Deliveries. GET, POST,PUT, DELETE</h4>

<h2>API Route Using DRF (Django Rest Framework)</h2>
<h4>Api route for all matches. GET, POST,PUT, DELETE</h4>
<h4>Api route for all Deliveries. GET, POST,PUT, DELETE</h4>

<h1>Specificaion:</h1>
<h4>Python 3.7</h4>
<h4>Database- PostgreSQL</h4>
<h4>IDE- VS Code</h4>
<h4>For plotting-</h4>
<h4>Highchart</h4>